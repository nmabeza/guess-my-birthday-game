from random import randint

#use randint function to get a random number between two values number_between_whatever = randint(x,y)
#ask user for name

name = input("What is your name?")
print("Hi", name)

#up to 5 times guess birth month and year
#only guess between 1924 and 2004

guess_month = randint(1,12)
guess_year = randint(1924,2004)

guess_counter = 1


while guess_counter < 5:
    print("Guess ", guess_counter, ":" , name, "were you born in",
    guess_month, "/", guess_year,"?")
#"Guess: <name> were you borin in <m>/<yyyy>
#prompt yes or no
    answer = input("yes or no?")
    if answer == "yes":
        print("I knew it!")
        exit()
    else:
        print("Drat! Let me try again")
        guess_counter +=1
print("I have other things to do!")
#if yes, print "I knew it!"

#if no print "Drat! Let me try again"

# on 5th try print "I have other things to do. Good bye"
